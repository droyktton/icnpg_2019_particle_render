#ifndef GLSYSTEM_H
#define GLSYSTEM_H

#include <GL/glew.h>
#include <GL/freeglut.h>

#ifndef CPU

#include <cuda_gl_interop.h>
#include <cuda_runtime.h>
#include <thrust/for_each.h>
#include <thrust/iterator/counting_iterator.h>

#endif

#include<iostream>
#include<cstdlib>
#include<ctime>
#include <cmath>
#include "posicioner.h"

class GLsystem: public virtual posicioner
{
  int L, N;

public:
    GLsystem(int N, int _L);
    ~GLsystem();
    void update_posotions_and_colors();
};

#endif // GLSYSTEM_H
