// OpenGL Graphics includes
#include <GL/glew.h>
#include <GL/gl.h>

#include <GL/freeglut.h>

// Includes
#include <iostream>
#include<stdio.h>
#include<ctype.h>
#include<stdlib.h>
#include <getopt.h>
#include <cstdlib>
#include <cstdio>
#include <algorithm>

#include "render_particles.h"
#include "glsystem.h"

#define SIZE 1000
#define N_PART 10000

const uint width = 640, height = 480;

ParticleRenderer *renderer = 0;
GLsystem *xy_model = 0;

// view params
int ox, oy;

float camera_trans[] = {0, 0, -2*SIZE};
float camera_rot[]   = {0, 0, 0};
float camera_trans_lag[] = {0, 0, -3};
float camera_rot_lag[] = {0, 0, 0};
const float inertia = 0.1f;

bool displayEnabled = true;


//ParticleRenderer::DisplayMode displayMode = ParticleRenderer::PARTICLE_SPHERES;
ParticleRenderer::DisplayMode displayMode = ParticleRenderer::PARTICLE_SPRITES;
//ParticleRenderer::DisplayMode displayMode = ParticleRenderer::PARTICLE_POINTS;


float modelView[16];


float particleRadius = float(SIZE/200);

void initGL(int argc, char **argv)
{
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
    glutInitWindowSize(width, height);
    glutCreateWindow("CUDA Particles");

    glewInit();

    glEnable(GL_DEPTH_TEST);
    glClearColor(0.25, 0.25, 0.25, 1.0);
    //glClearColor(0.0, 0.0, 0.0, 1.0);

    glutReportErrors();
    printf("init system\n");
    //xy_model = new GLsystem(parms);
    xy_model = new GLsystem(N_PART, SIZE);
    xy_model->update_posotions_and_colors();
    renderer = new ParticleRenderer;
    renderer->setParticleRadius(particleRadius);
    //LLENAR ACA CON EL BUFFER DE COLOR DEL XY
    renderer->setColorBuffer(xy_model->get_color_buf());
    printf("color buffer setted\n");
}

void display()
{

    if (renderer)
    {
        //CALCULAR LAS POSICIONES, PASAR EL BUFFER Y EL NUMERO DE PARTICULAS
        renderer->setVertexBuffer(xy_model->get_current_read_buffer(), xy_model->get_number_of_particles());
    }

    // render
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // view transform
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    for (int c = 0; c < 3; ++c)
    {
        camera_trans_lag[c] += (camera_trans[c] - camera_trans_lag[c]) * inertia;
        camera_rot_lag[c] += (camera_rot[c] - camera_rot_lag[c]) * inertia;
    }

    glTranslatef(camera_trans_lag[0], camera_trans_lag[1], camera_trans_lag[2]);
    glRotatef(camera_rot_lag[0], 1.0, 0.0, 0.0);
    glRotatef(camera_rot_lag[1], 0.0, 1.0, 0.0);

    glGetFloatv(GL_MODELVIEW_MATRIX, modelView);

    //glColor3f(1.0, 1.0, 1.0);
    //draweRect();
    //glutWireCube(parms.Lxy);

    if (renderer){
       renderer->display(displayMode);
      }
    
    glutSwapBuffers();
    glutReportErrors();

}

void idleFunc(void)
{
    display();
}

void reshape(int w, int h)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, (float) w / (float) h, 0.1, 4*SIZE);

    glMatrixMode(GL_MODELVIEW);
    glViewport(0, 0, w, h);

    if (renderer)
    {
        renderer->setWindowSize(w, h);
        renderer->setFOV(60.0);
    }
}


void mouse(int button, int state, int x, int y){
 if ( (button == GLUT_LEFT_BUTTON) & (state == GLUT_DOWN) ) {
 ox = x; oy = y;
 }
 if ((button == 3) || (button == 4)) // It's a wheel event
  {
      // Each wheel event reports like a button click, GLUT_DOWN then GLUT_UP
      if (state == GLUT_UP) return; // Disregard redundant GLUT_UP events
      switch (button) {
        case 3:
          camera_trans[2] += SIZE/20;
          break;
        case 4:
          camera_trans[2] -= SIZE/20;
          break;
        }
  }
}

void motion(int x, int y) {
 camera_rot[0] += (y - oy);
 camera_rot[1] += (x - ox);
 ox = x; oy = y;
 glutPostRedisplay();
}

void key(unsigned char key, int /*x*/, int /*y*/)
{
    switch (key)
    {
      case ' ':
        xy_model->update_posotions_and_colors();
        break;
      case 'n':
        camera_trans[2] += SIZE/20;
        break;
      case 'm':
        camera_trans[2] -= SIZE/20;
        break;
      case 'j':
        camera_rot[1] -= 3.0;
        break;
      case 'l':
        camera_rot[1] += 3.0;
        break;
      case 'i':
        camera_rot[0] += 3.0f;
        break;
      case 'k':
        camera_rot[0] -= 3.0f;
        break;
      case 'r':
        displayMode = ParticleRenderer::DisplayMode::PARTICLE_SPHERES;
        break;
      case 't':
        displayMode = ParticleRenderer::DisplayMode::PARTICLE_SPRITES;
        break;
      case 'y':
        displayMode = ParticleRenderer::DisplayMode::PARTICLE_POINTS;
        break;
      case 'f':
        particleRadius += 0.1f;
        renderer->setParticleRadius(particleRadius);
        break;
      case 'g':
        particleRadius -= 0.1f;
        renderer->setParticleRadius(particleRadius);
        break;
    }
    glutPostRedisplay();
}

////////////////////////////////////////////////////////////////////////////////
// Program main
////////////////////////////////////////////////////////////////////////////////
int
main(int argc, char **argv)
{
#if defined(__linux__)
    setenv ("DISPLAY", ":0", 0);
#endif
    printf("NOTE: The CUDA Samples are not meant for performance measurements. Results may vary when GPU Boost is enabled.\n\n");
    initGL(argc, argv);

    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutIdleFunc(idleFunc);
    glutMouseFunc(mouse);
    glutMotionFunc(motion);
    glutKeyboardFunc(key);
    //glutSpecialFunc(special);
    //glutCloseFunc(cleanup);
    glutMainLoop();

    return 0;
}

