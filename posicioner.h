#ifndef POSICIONER_H
#define POSICIONER_H

#include <GL/glew.h>
#include <GL/freeglut.h>

#ifndef CPU

#include <cuda_gl_interop.h>
#include <cuda_runtime.h>
#include <thrust/for_each.h>
#include <thrust/iterator/counting_iterator.h>

#endif

#include<iostream>
#include<cstdlib>
#include<ctime>
#include <cmath>


class posicioner
{
protected:
  uint posVbo;
  uint colorVBO;
  uint buffer_size;
  uint current_part_num;
#ifndef CPU
  struct cudaGraphicsResource *m_cuda_posvbo_resource; // handles OpenGL-CUDA exchange
  struct cudaGraphicsResource *m_cuda_colorvbo_resource; // handles OpenGL-CUDA exchange
#endif

public:
    posicioner(int N);
    ~posicioner();
    virtual void update_positions_and_colors(){}

    uint get_current_read_buffer(){
        return posVbo;
    }
    uint get_color_buf(){
        return colorVBO;
    }
    uint get_number_of_particles(){
        return current_part_num;
    }
protected:
    void _init_openGL();
    void _finalize_openGL();
    uint createVBO(uint size);
};

#ifndef CPU

extern "C"
{
    void registerGLBufferObject(uint vbo, struct cudaGraphicsResource **cuda_vbo_resource);
    void unregisterGLBufferObject(struct cudaGraphicsResource *cuda_vbo_resource);
    void *mapGLBufferObject(struct cudaGraphicsResource **cuda_vbo_resource);
    void unmapGLBufferObject(struct cudaGraphicsResource *cuda_vbo_resource);
}
#endif


#endif // POSICIONER_H
