#include "glsystem.h"
#include "Random123/philox.h"
#include "Random123/u01.h"

#define PI 3.141592654f

typedef r123::Philox2x32 RNG;

#ifdef CPU
struct float2
{
    float x, y;
};
#endif

template<class T>
#ifndef CPU
__device__
#endif
constexpr const T& clamp( const T& v, const T& lo, const T& hi )
{
    //assert( !(hi < lo) );
    return (v < lo) ? lo : (hi < v) ? hi : v;
}

#ifndef CPU
__device__
#endif
float2 uniform(int n, int seed, int t)
{   /*
    Generador de numeros aleatorios en la memoria individual de cada Tread.
    Recive tres enteros de argumento y retorna una tupla (tipo float2 de CUDA) de dos
    nueros aleatoreo "unicos" para una dada terna de enteros.
    El resultado puede ser regenerado en cualquier momento con la misma terna de enteros.
    */

    // keys and counters
    RNG philox;
    RNG::ctr_type c={{}};
    RNG::key_type k={{}};
    RNG::ctr_type r;
    // Garantiza una secuencia random "unica" para cada thread
    k[0] = n;
    c[1] = seed;
    c[0] = t;
    r = philox(c, k);
    float2 num;
    num.x = u01_closed_closed_32_53(r[0]);
    num.y = u01_closed_closed_32_53(r[1]);
    return num;
}

#ifndef CPU
struct GenRand
{
    int L, seed;
    float * postitions;
    float * colors;

    GenRand(int _L, int _seed, float * _pos, float *_col) :
        L(_L), seed(_seed),
        postitions(_pos),
        colors(_col){}

    __device__
    void operator () (int n)
    {
        float R, phi, theta, r, g, b;
        float2 rn1 = uniform(n, seed, 0);
        float2 rn2 = uniform(n, seed, 1);
        float2 rn3 = uniform(n, seed, 2);
#ifdef SPHERE
#ifdef WRONG
            phi = rn1.y;
            R = L*rn1.x;
#else
            R = L*std::cbrt(rn1.x);
            phi = acos(2.0*rn1.y-1.0);
#endif
            theta = 2*PI*rn2.x;
            postitions[4*n] = R*sin(phi)*cos(theta);
            postitions[4*n+1] = R*cos(phi);
            postitions[4*n+2] = R*sin(phi)*sin(theta);
#else
        postitions[4*n] = L*rn1.x -L/2;
        postitions[4*n+1] = L*rn1.y -L/2;
        postitions[4*n+2] = L*rn2.x -L/2;
#endif
        postitions[4*n+3] = 1.0;
#ifdef CMAP
#ifdef UNIFORM
            float xx = rn1.y;
            r = clamp(8.0 / 3.0 * xx, 0.0, 1.0);
            g = clamp(8.0 / 3.0 * xx-1.0, 0.0, 1.0);
            b = clamp(4.0 * xx-3.0, 0.0, 1.0);
#else
            phi = rn2.y;
            r = clamp(8.0 / 3.0 * phi, 0.0, 1.0);
            g = clamp(8.0 / 3.0 * phi-1.0, 0.0, 1.0);
            b = clamp(4.0 * phi-3.0, 0.0, 1.0);
#endif
#else
            r = rn2.y;
            g = rn3.x;
            b = rn3.y;
#endif
        colors[4*n] = r;
        colors[4*n+1] = g;
        colors[4*n+2] = b;
        colors[4*n+3] = 1.0;
    }
};
#endif

GLsystem::GLsystem(int N, int _L) : posicioner(N),
    L(_L), N(N)
{
}


GLsystem::~GLsystem()
{
}

void GLsystem::update_posotions_and_colors()
{
    int seed = time(0);

#ifndef CPU
    float *positions;
    float *colors;

    positions = (float *) mapGLBufferObject(&m_cuda_posvbo_resource);
    colors = (float *) mapGLBufferObject(&m_cuda_colorvbo_resource);

    GenRand compute(L, seed, positions, colors);

    thrust::for_each(
                thrust::make_counting_iterator(0),
                thrust::make_counting_iterator(int(N)),
                compute
                );

    unmapGLBufferObject(m_cuda_posvbo_resource);
    unmapGLBufferObject(m_cuda_colorvbo_resource);
#else
    glBindBuffer(GL_ARRAY_BUFFER, posVbo);
    float *positions = (float*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
    //float* ptr = data;
    float R, phi, theta;
    if (positions){
        for (int i = 0; i < current_part_num; ++i)
        {
#ifdef SPHERE
#ifdef WRONG
            phi = PI*uniform(i, seed, 0).y;
            R = L*uniform(i, seed, 0).x;
#else
            R = L*std::cbrt(uniform(i, seed, 0).x);
            phi = acos(2.0*uniform(i, seed, 0).y-1.0);
#endif
            theta = 2*PI*uniform(i, seed, 1).x;

            *positions++ = R*sin(phi)*sin(theta);
            *positions++ = R*cos(phi);
            *positions++ = R*sin(phi)*cos(theta);
#else
            *positions++ = L*uniform(i, seed, 0).x -L/2;
            *positions++ = L*uniform(i, seed, 0).y -L/2;
            *positions++ = L*uniform(i, seed, 1).x -L/2;

#endif
            *positions++ = 1.0;
        }
        glUnmapBuffer(GL_ARRAY_BUFFER);
    }

    glBindBuffer(GL_ARRAY_BUFFER, colorVBO);
    float *colors = (float*)glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
    float r, g, b;
    if (colors){
        for (int i = 0; i < current_part_num; ++i)
        {
#ifdef CMAP
#ifdef UNIFORM
            float xx = uniform(i, seed, 0).y;
            r = clamp(8.0 / 3.0 * xx, 0.0, 1.0);
            g = clamp(8.0 / 3.0 * xx-1.0, 0.0, 1.0);
            b = clamp(4.0 * xx-3.0, 0.0, 1.0);
#else
            phi = uniform(i, seed, 1).y;
            r = clamp(8.0 / 3.0 * phi, 0.0, 1.0);
            g = clamp(8.0 / 3.0 * phi-1.0, 0.0, 1.0);
            b = clamp(4.0 * phi-3.0, 0.0, 1.0);
#endif
#else
            r = uniform(i, seed, 1).y;
            g = uniform(i, seed, 2).x;
            b = uniform(i, seed, 2).y;
#endif
            *colors++ = r;
            *colors++ = g;
            *colors++ = b;
            *colors++ = 1.0;
        }
        glUnmapBuffer(GL_ARRAY_BUFFER);
    }
#endif
}
